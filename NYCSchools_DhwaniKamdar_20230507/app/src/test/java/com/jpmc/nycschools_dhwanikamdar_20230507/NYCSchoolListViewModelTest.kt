package com.jpmc.nycschools_dhwanikamdar_20230507

import com.jpmc.nycschools_dhwanikamdar_20230507.datamodel.NYCSchool
import com.jpmc.nycschools_dhwanikamdar_20230507.repositories.NYCSchoolsRepository
import com.jpmc.nycschools_dhwanikamdar_20230507.viewmodel.NYCSchoolListViewModel
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.flow.flow
import org.junit.Before
import org.junit.Test
import org.junit.jupiter.api.Assertions.*
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner

/**
@RunWith(RobolectricTestRunner::class) - annotation used in JUnit tests in Android development that specifies which runner to use for executing the tests.
In this case, RobolectricTestRunner is used as the runner.

RobolectricTestRunner - provides faster testing of the NYCSchoolListViewModel class

TEST NYCSchoolListViewModel - tests various functions of the NYCSchoolListViewModel using mock objects to simulate the repository and data.

    testFetchSchools_returnsResults() - tests if the fetchSchools() function returns expected data and saves it in the viewModel's schoolListLiveData.

    testLoadMore_loadsAdditionalData() -  tests if the loadMore() function adds additional data to the viewModel's cache.

    testRefreshData() - tests if the refreshData() function reloads data into the viewModel's schoolListLiveData.

    testSearchSchool_returnsNonEmptySearchResults() -  tests if the searchSchool() function returns non-empty search results for a given search query.

    testSearchSchool_returnsEmptySearchResults() - tests if the searchSchool() function returns an empty list for an invalid search query.

    testFilterSchool_returnsNonEmptyFilteredResult() - tests if the filterSchool() function returns a non-empty list of filtered results for a given search query.

    testFilterSchool_returnsEmptyFilteredResult() - tests if the filterSchool() function returns an empty list for an invalid search query.
 */

@RunWith(RobolectricTestRunner::class)
internal class NYCSchoolListViewModelTest {

    private lateinit var viewModel: NYCSchoolListViewModel
    private val repo: NYCSchoolsRepository = mockk()
    private val school: NYCSchool = mockk()

    @Before
    fun setUp() {
        coEvery { repo.fetchSchoolList(any()) } returns flow { emit(ArrayList<NYCSchool>().apply { add(school) }) }
        viewModel = NYCSchoolListViewModel(repo)
    }

    @Test
    fun testFetchSchools_returnsResults() {
        viewModel.fetchSchools()

        coEvery { repo.fetchSchoolList(any()) }
        assertNotNull(viewModel.schoolListLiveData.value)
        assertEquals(1, viewModel.schoolListLiveData.value?.size)
        assertEquals(school, viewModel.schoolListLiveData.value?.get(0))
    }

    @Test
    fun testLoadMore_loadsAdditionalData() {
        viewModel.fetchSchools()

        viewModel.loadMore()

        assertNotNull(viewModel.schoolListLiveData.value)
        assertEquals(1, viewModel.schoolListLiveData.value?.size)
        assertEquals(2, viewModel.cache.size)
        assertEquals(school, viewModel.cache[0])
        assertEquals(school, viewModel.cache[1])
    }

    @Test
    fun testRefreshData() {
        viewModel.fetchSchools()

        viewModel.refreshData()

        assertNotNull(viewModel.schoolListLiveData.value)
        assertEquals(1, viewModel.schoolListLiveData.value?.size)
        assertEquals(school, viewModel.schoolListLiveData.value?.get(0))
    }

    @Test
    fun testSearchSchool_returnsNonEmptySearchResults() {
        val schoolName = "School name"
        coEvery { school.schoolName } returns schoolName
        coEvery { repo.fetchSchoolByName(any()) } returns flow { emit(ArrayList<NYCSchool>().apply { add(school) }) }

        viewModel.searchSchool(schoolName)

        assertNotNull(viewModel.searchSchoolLiveData.value)
        assertEquals(viewModel.searchSchoolLiveData.value?.isNotEmpty(), true)
    }

    @Test
    fun testSearchSchool_returnsEmptySearchResults() {
        val schoolName = "School name"
        coEvery { school.schoolName } returns schoolName
        coEvery { repo.fetchSchoolByName(any()) } returns flow { emit(emptyList() ) }

        viewModel.searchSchool("query")

        assertNotNull(viewModel.searchSchoolLiveData.value)
        assertEquals(viewModel.searchSchoolLiveData.value?.isEmpty(), true)
    }

    @Test
    fun testFilterSchool_returnsNonEmptyFilteredResult() {
        val schoolName = "School name"
        coEvery { school.schoolName } returns schoolName
        viewModel.fetchSchools()

        viewModel.filterSchool(schoolName)

        assertNotNull(viewModel.cache)
        assertEquals(viewModel.searchSchoolLiveData.value?.isNotEmpty(), true)
    }

    @Test
    fun testFilterSchool_returnsEmptyFilteredResult() {
        val schoolName = "School name"
        coEvery { school.schoolName } returns schoolName
        viewModel.fetchSchools()

        viewModel.filterSchool("query")

        assertNotNull(viewModel.cache)
        assertEquals(viewModel.searchSchoolLiveData.value?.isEmpty(), true)
    }
}
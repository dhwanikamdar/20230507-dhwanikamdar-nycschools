package com.jpmc.nycschools_dhwanikamdar_20230507.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.jpmc.nycschools_dhwanikamdar_20230507.parent.ParentViewModel

import com.jpmc.nycschools_dhwanikamdar_20230507.datamodel.SatScore
import com.jpmc.nycschools_dhwanikamdar_20230507.repositories.NYCSchoolsRepository
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.launch

class NYCSchoolScoresViewModel(repository: NYCSchoolsRepository) : ParentViewModel(repository) {
    val schoolScoresLiveData: MutableLiveData<List<SatScore>> = MutableLiveData()

    fun fetchSchoolScores(id: String) {
        if (schoolScoresLiveData.value == null) {
            viewModelScope.launch {
                repository.fetchSchoolScoreById(id).catch {
                    schoolScoresLiveData.value = ArrayList()
                }.collect { schoolScoresLiveData.value = it }
            }
        }
    }
}
package com.jpmc.nycschools_dhwanikamdar_20230507.api

import com.jpmc.nycschools_dhwanikamdar_20230507.util.Config
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

/**
Singleton class - that provides a Retrofit client instance with the necessary configurations for making API requests to the NYCSchoolsApi interface.

OkHttpClient instance - to add an interceptor for logging HTTP requests and responses.
The retrofit instance - created with the Retrofit.Builder()
                      - configured with a base URL for the API

GsonConverterFactory  - for parsing JSON responses
CallAdapterFactory    - for converting Retrofit calls to RxJava 2 observables.

createRestApi() - creates an instance of the specified service class with the retrofit instance.
                  returns the service instance, which is used to make API requests to the NYCSchoolsApi interface
 */

object NYCSchoolAPIProvider {

    private val retrofit: Retrofit
    private val okHttpClient: OkHttpClient.Builder = OkHttpClient.Builder()

    init {
        okHttpClient.addInterceptor(HttpLoggingInterceptor().apply { setLevel(HttpLoggingInterceptor.Level.BODY) })
        retrofit = Retrofit.Builder().baseUrl(Config.SCHOOL_BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(
                RxJava2CallAdapterFactory.create()
            ).client(okHttpClient.build()).build()

    }

    @JvmStatic
    fun <S> createRestApi(service: Class<S>): S {
        return retrofit.create(service)
    }
}
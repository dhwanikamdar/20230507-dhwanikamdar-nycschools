package com.jpmc.nycschools_dhwanikamdar_20230507.parent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.DrawableRes
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.jpmc.nycschools_dhwanikamdar_20230507.viewmodel.NYCSchoolViewModelFactory

/**
ParentFragment - provides common implementation for fragments

onCreate() -  called when the fragment is first created.

onCreateView() - called to create and return the view hierarchy associated with the fragment.
observeViewModel() - called to observe changes in the ViewModel
initFragmentView() - is called to initialize the fragment view.
initFragmentView() -  returns the root view for the fragment.

onViewCreated() - called after the view is created.

getActionBarViewData() - an abstract method that returns an ActionBarViewData object - implemented in subclasses of ParentFragment.

observeViewModel() - an abstract method that observes changes in the ViewModel - implemented in subclasses of ParentFragment.

initFragmentView() -  abstract method that initializes the fragment view - implemented in subclasses of ParentFragment.

ActionBarViewData() -  data class is used to hold the data needed to configure the action bar in onViewCreated method.
 */

abstract class ParentFragment : Fragment() {
    val viewModelFactory: NYCSchoolViewModelFactory = NYCSchoolViewModelFactory()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        observeViewModel()
        return initFragmentView(inflater)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as AppCompatActivity).supportActionBar?.apply {
            setDisplayHomeAsUpEnabled(true)
            setHomeAsUpIndicator(getActionBarViewData().actionBarHomeIcon)
            title = getActionBarViewData().actionBarTitle
        }
    }

    abstract fun getActionBarViewData(): ActionBarViewData

    abstract fun observeViewModel()

    abstract fun initFragmentView(inflater: LayoutInflater): View

    data class ActionBarViewData(
        val actionBarTitle: String,
        @DrawableRes val actionBarHomeIcon: Int
    )
}
package com.jpmc.nycschools_dhwanikamdar_20230507.parent
import androidx.lifecycle.ViewModel
import com.jpmc.nycschools_dhwanikamdar_20230507.repositories.NYCSchoolsRepository


/**
abstract class - provides a base class for other view model classes in the application.

The constructor of this abstract class takes an object of NYCSchoolsRepository class as a parameter.
This repository object will be used to interact with the data layer of the application.

The abstract class provides a basic structure for implementing the view model functionality by defining a single parameterized constructor and providing a reference to the NYCSchoolsRepository instance.
It also provides the basic functionality of the ViewModel class by extending it.

The actual implementation of the functionality will be provided in the derived classes of the ParentViewModel class.
Since this class is abstract, the derived classes must implement all the abstract functions.
This class serves as a common base class for all the view model classes that use the NYCSchoolsRepository object.
 */

abstract class ParentViewModel(protected val repository: NYCSchoolsRepository) : ViewModel()
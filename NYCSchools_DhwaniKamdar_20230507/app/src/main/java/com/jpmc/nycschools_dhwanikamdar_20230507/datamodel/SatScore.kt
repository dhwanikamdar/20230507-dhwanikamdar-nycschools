package com.jpmc.nycschools_dhwanikamdar_20230507.datamodel

import com.google.gson.annotations.SerializedName

/*
data class SatScore(
    val dbn: String,
    val schoolName: String,
    val numOfSatTestTakers: Int,
    val satCriticalReadingAvgScore: Int,
    val satMathAvgScore: Int,
    val satWritingAvgScore: Int
)
*/

data class SatScore(
    @SerializedName("dbn") val id: String,
    @SerializedName("school_name") val schoolName: String,
    @SerializedName("num_of_sat_test_takers") val satTakers: String,
    @SerializedName("sat_math_avg_score") val satMath: String,
    @SerializedName("sat_writing_avg_score") val satWriting: String,
    @SerializedName("sat_critical_reading_avg_score") val satReading: String
)

package com.jpmc.nycschools_dhwanikamdar_20230507.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.jpmc.nycschools_dhwanikamdar_20230507.api.NYCSchoolAPI

import com.jpmc.nycschools_dhwanikamdar_20230507.datamodel.NYCSchool
import com.jpmc.nycschools_dhwanikamdar_20230507.datamodel.SatScore
import com.jpmc.nycschools_dhwanikamdar_20230507.repositories.NYCSchoolsRepository
import kotlinx.coroutines.launch
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class NYCSchoolViewModel() : ViewModel() {

    private val api = Retrofit.Builder()
        .baseUrl("https://data.cityofnewyork.us/")
        .addConverterFactory(GsonConverterFactory.create())
        .build()
        .create(NYCSchoolAPI::class.java)

    private val repository =  NYCSchoolsRepository

    private val _schools = MutableLiveData<List<NYCSchool>>()
    val schools: LiveData<List<NYCSchool>> = _schools

    private val _selectedSchool = MutableLiveData<NYCSchool>()
    val selectedSchool: LiveData<NYCSchool> = _selectedSchool

    private val _satScores = MutableLiveData<SatScore>()
    val satScores: LiveData<SatScore> = _satScores

    private val _loading = MutableLiveData<Boolean>()
    val loading: LiveData<Boolean> = _loading

    private val _error = MutableLiveData<String>()
    val error: LiveData<String> = _error

      /*fun getNycSchoolsList() {
        viewModelScope.launch {
            _loading.value = true
            try {
                val result = repository.getNycSchools()
                _schools.value = result
                _loading.value = false
            } catch (e: Exception) {
                _error.value = e.message
                _loading.value = false
            }
        }
    }*/


    /*fun onNycSchoolSelected(school: NYCSchool) {
        _selectedSchool.value = school
        viewModelScope.launch {
            _loading.value = true
            try {
                _satScores.value = repository.getSatScoresBySchool(school.dbn)
                _loading.value = false
            }catch (e: Exception) {
                _error.value = e.message
                _loading.value = false
            }
        }
    }*/

    fun selectSchool(school: NYCSchool) {
        _selectedSchool.value = school
    }
}
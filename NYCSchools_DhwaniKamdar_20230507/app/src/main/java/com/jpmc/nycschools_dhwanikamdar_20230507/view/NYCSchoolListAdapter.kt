package com.jpmc.nycschools_dhwanikamdar_20230507.view

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.jpmc.nycschools_dhwanikamdar_20230507.databinding.NycSchoolItemBinding
import com.jpmc.nycschools_dhwanikamdar_20230507.datamodel.NYCSchool
import com.jpmc.nycschools_dhwanikamdar_20230507.util.NYCSchoolItemActionListener

/**
RecyclerView adapter - displays a list of NYCSchool objects.
The class has a constructor that takes an ArrayList of NYCSchool objects and an interface called NYCSchoolItemActionListener.
The adapter uses a view holder pattern, which is defined by the inner class SchoolViewHolder, to display each NYCSchool object in a RecyclerView.

SchoolViewHolder() - binds the properties of a NYCSchool object to a view using data binding.
It also sets up click listeners for various views within the item view, such as call, website, email, and map.

The adapter also provides two methods for updating the list of NYCSchool objects - addSchools() and setSchoolList().
addSchools() -  adds a list of NYCSchool objects to the existing list and notifies the adapter that items have been added.
setSchoolList() - replaces the entire list of NYCSchool objects and notifies the adapter that the data set has changed.

The adapter overrides three methods - onCreateViewHolder(), onBindViewHolder(), and getItemCount().
onCreateViewHolder() - inflates the layout for each item in the RecyclerView.
onBindViewHolder() -  binds each NYCSchool object to its corresponding view holder.
getItemCount() -  returns the number of items in the list of NYCSchool objects.
 */

class NYCSchoolListAdapter(
    private var schools: ArrayList<NYCSchool>,
    private val itemClickListener: NYCSchoolItemActionListener
) :
    RecyclerView.Adapter<NYCSchoolListAdapter.SchoolViewHolder>() {

    class SchoolViewHolder(private val itemBinding: NycSchoolItemBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {
        fun bind(item: NYCSchool, itemClickListener: NYCSchoolItemActionListener) {
            itemBinding.schoolName.text = item.schoolName
            itemBinding.location.text = item.address
            itemBinding.startTime.text = item.startTime
            itemBinding.endTime.text = item.endTime
            itemBinding.call.setOnClickListener { v ->
                itemClickListener.onSchoolItemClicked(
                    view = v,
                    school = item
                )
            }
            itemBinding.website.setOnClickListener { v ->
                itemClickListener.onSchoolItemClicked(
                    view = v,
                    school = item
                )
            }
            itemBinding.email.setOnClickListener { v ->
                itemClickListener.onSchoolItemClicked(
                    view = v,
                    school = item
                )
            }
            itemBinding.map.setOnClickListener { v ->
                itemClickListener.onSchoolItemClicked(
                    view = v,
                    school = item
                )
            }
            itemView.setOnClickListener { v ->
                itemClickListener.onSchoolItemClicked(
                    view = v,
                    school = item
                )
            }
        }
    }

    fun addSchools(schoolList: List<NYCSchool>) {
        val size = schools.size
        schools.addAll(schoolList)
        notifyItemRangeChanged(size-1, schools.size)
    }

    @SuppressLint("NotifyDataSetChanged") // Refreshing the whole list
    fun setSchoolList(schoolList: List<NYCSchool>) {
        schools = ArrayList(schoolList)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SchoolViewHolder {
        val binding: NycSchoolItemBinding =
            NycSchoolItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return SchoolViewHolder(binding)
    }

    override fun onBindViewHolder(holder: SchoolViewHolder, position: Int) {
        val school = schools[position]
        holder.bind(school, itemClickListener)
    }

    override fun getItemCount(): Int {
        return schools.size
    }
}
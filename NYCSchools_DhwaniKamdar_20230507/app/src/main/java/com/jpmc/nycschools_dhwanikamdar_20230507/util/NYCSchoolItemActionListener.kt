package com.jpmc.nycschools_dhwanikamdar_20230507.util

import android.view.View
import com.jpmc.nycschools_dhwanikamdar_20230507.datamodel.NYCSchool


/**
    NYCSchoolItemActionListener - interface is implemented by a class that needs to listen to the actions performed on an item in a RecyclerView.
    The interface has a single method named onSchoolItemClicked, which is called when the user clicks on a school item in the RecyclerView.
    The method takes two parameters: a View object representing the item that was clicked, and a NYCSchool object representing the school associated with the clicked item.
 */
interface NYCSchoolItemActionListener {

    fun onSchoolItemClicked(view: View, school: NYCSchool)
}
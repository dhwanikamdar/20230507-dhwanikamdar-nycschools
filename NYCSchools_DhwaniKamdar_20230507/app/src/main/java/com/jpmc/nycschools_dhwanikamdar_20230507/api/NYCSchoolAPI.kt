package com.jpmc.nycschools_dhwanikamdar_20230507.api

import com.google.gson.annotations.SerializedName
import com.jpmc.nycschools_dhwanikamdar_20230507.datamodel.NYCSchool
import com.jpmc.nycschools_dhwanikamdar_20230507.datamodel.SatScore
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.QueryMap


/**
 NYCSchools API - the endpoints for making API requests to retrieve a list of NYC high schools and their SAT scores.

 getSchoolList(): A GET request method with a query parameter to retrieve a list of high schools based on the specified filters.
                  takes a Map of filters as a parameter
                  returns a Response object containing a list of NYCSchool objects.

 getSchoolScoreById(): A GET request method with a query parameter to retrieve the SAT scores for a particular high school based on the specified filters.
                       takes in a Map of filters as a parameter
                       returns a Response object containing a list of SatScore objects.

Additionally, two inner classes -  NYCHighSchoolResponse and SATScoreResponse - can be used later with Response objects.
use @SerializedName annotations to map the JSON keys from the API response to their respective fields in the data classes.

 */


interface NYCSchoolAPI {

    @GET("s3k6-pzi2.json")
    suspend fun getSchoolList(@QueryMap(encoded = true) filters: Map<String, String>): Response<List<NYCSchool>>

    @GET("f9bf-2cp4.json")
    suspend fun getSchoolScoreById(@QueryMap(encoded = true) filters: Map<String, String>): Response<List<SatScore>>


    data class NYCHighSchoolResponse(
        @SerializedName("dbn") val dbn: String,
        @SerializedName("school_name") val schoolName: String,
        @SerializedName("overview_paragraph") val overviewParagraph: String,
        @SerializedName("location") val location: String,
        @SerializedName("phone_number") val phoneNumber: String,
        @SerializedName("school_email") val schoolEmail: String,
        @SerializedName("website") val website: String,
        @SerializedName("total_students") val totalStudents: String
    )

    data class SATScoreResponse(
        @SerializedName("dbn") val dbn: String,
        @SerializedName("school_name") val schoolName: String,
        @SerializedName("num_of_sat_test_takers") val numOfSatTestTakers: Int,
        @SerializedName("sat_critical_reading_avg_score") val satCriticalReadingAvgScore: Int,
        @SerializedName("sat_math_avg_score") val satMathAvgScore: Int,
        @SerializedName("sat_writing_avg_score") val satWritingAvgScore: Int
    )


}


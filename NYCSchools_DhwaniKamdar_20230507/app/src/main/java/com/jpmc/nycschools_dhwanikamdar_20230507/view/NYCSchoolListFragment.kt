package com.jpmc.nycschools_dhwanikamdar_20230507.view

import android.app.SearchManager
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.View
import android.widget.ImageView
import android.widget.SearchView
import android.widget.SearchView.OnQueryTextListener
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.OnScrollListener
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.jpmc.nycschools_dhwanikamdar_20230507.parent.ParentFragment
import com.jpmc.nycschools_dhwanikamdar_20230507.databinding.NycSchoolListFragmentBinding
import com.jpmc.nycschools_dhwanikamdar_20230507.datamodel.NYCSchool
import com.jpmc.nycschools_dhwanikamdar_20230507.util.NYCSchoolItemActionListener
import com.jpmc.nycschools_dhwanikamdar_20230507.viewmodel.NYCSchoolListViewModel
import com.jpmc.nycschools_dhwanikamdar_20230507.R

/**
NYCSchoolListFragment - displays a list of schools.
The class uses a ViewModel to manage its data and updates the UI accordingly.


onCreate() - called when the fragment is created. It initializes the ViewModel, sets the mode to SurfaceMode.DEFAULT or SurfaceMode.SEARCH depending on whether there is a search query, and restores the search query if it exists.
onViewCreated() - called when the view is created. It sets up the RecyclerView, the adapter, and the swipe refresh layout, and fetches the schools from the ViewModel.
initFragmentView() - inflates the layout for the fragment and returns the root view.
getActionBarViewData() - returns data for the action bar.
onCreateOptionsMenu() - called to create the options menu for the fragment. It inflates the search menu and sets up the search view with a search manager and a query text listener. It also handles the click event for the search close button and restores the search query if it exists.
onRefresh() - called when the user pulls down on the swipe refresh layout. It refreshes the schools by fetching them from the ViewModel.
getPhoneIntent() - takes a NYCSchool object and returns an intent for calling the school's phone number.
getWebsiteIntent() - takes a NYCSchool object and returns an intent for opening the school's website.
getDirectionsIntent() - takes a NYCSchool object and returns an intent for opening Google Maps with the school's address.
getEmailIntent() - takes a NYCSchool object and returns an intent for composing an email to the school.
startActivitySafely() - takes an intent and starts the activity if it can be resolved by the system without throwing an ActivityNotFoundException.
NYCSchoolItemActionListener() -interface for handling click events on a NYCSchool item.
It contains an onSchoolItemClicked function that handles the click event based on the view's ID, the corresponding intent is created and started using startActivitySafely.

 */

class NYCSchoolListFragment : ParentFragment(), SwipeRefreshLayout.OnRefreshListener {

    private lateinit var binding: NycSchoolListFragmentBinding
    private lateinit var viewModel: NYCSchoolListViewModel
    private var adapter: NYCSchoolListAdapter? = null
    private var mode: SurfaceMode = SurfaceMode.DEFAULT
    private lateinit var searchView : SearchView
    private var query : CharSequence? = null
    private val itemClickListener = object : NYCSchoolItemActionListener {
        override fun onSchoolItemClicked(view: View, school: NYCSchool) {
            when (view.id) {
                R.id.call -> startActivitySafely(getPhoneIntent(school))
                R.id.website -> startActivitySafely(getWebsiteIntent(school))
                R.id.map -> startActivitySafely(getDirectionsIntent(school))
                R.id.email -> startActivitySafely(getEmailIntent(school))
                else -> parentFragmentManager.beginTransaction().replace(
                    R.id.fragment_container,
                    NYCSchoolDetailsFragment.newInstance(school),
                    NYCSchoolDetailsFragment.TAG
                ).addToBackStack(null)
                    .commit()
            }
        }
    }
    private val listScrollListener = object : OnScrollListener() {
        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            super.onScrolled(recyclerView, dx, dy)
            val layoutManager = recyclerView.layoutManager as LinearLayoutManager
            if (!binding.swipeRefreshLayout.isRefreshing && mode == SurfaceMode.DEFAULT && layoutManager.findLastVisibleItemPosition() == (recyclerView.adapter?.itemCount?.minus(
                    1
                ) ?: -1)
            ) {
                binding.swipeRefreshLayout.isRefreshing = viewModel.canLoadMore()
                viewModel.loadMore()
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        query = savedInstanceState?.getCharSequence(SEARCH_QUERY_TEXT)
        mode = if (!TextUtils.isEmpty(query)) SurfaceMode.SEARCH else SurfaceMode.DEFAULT
        viewModel = ViewModelProvider(this, viewModelFactory)[NYCSchoolListViewModel::class.java]
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.recyclerView.layoutManager = LinearLayoutManager(context)
        adapter = NYCSchoolListAdapter(ArrayList(), itemClickListener)
        binding.recyclerView.adapter = adapter
        binding.recyclerView.setHasFixedSize(true)
        binding.recyclerView.addOnScrollListener(listScrollListener)
        binding.swipeRefreshLayout.setOnRefreshListener(this)
        binding.swipeRefreshLayout.isRefreshing = true
        if (mode == SurfaceMode.DEFAULT) {
            viewModel.fetchSchools()
        } else {
            viewModel.filterSchool(query.toString())
        }
    }

    override fun initFragmentView(inflater: LayoutInflater): View {
        binding = NycSchoolListFragmentBinding.inflate(inflater)
        return binding.root
    }

    override fun getActionBarViewData(): ActionBarViewData =
        ActionBarViewData(getString(R.string.school_list_title), R.drawable.ic_home)

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.search_menu, menu)
        val searchManager = activity?.getSystemService(Context.SEARCH_SERVICE) as SearchManager
        searchView = (menu.findItem(R.id.search).actionView) as SearchView
        searchView.apply {
            queryHint = getString(R.string.school_search_hint)
            setSearchableInfo(searchManager.getSearchableInfo(activity?.componentName))
            setOnQueryTextListener(searchQueryListener)
        }
        searchView.setOnQueryTextFocusChangeListener { _: View, hasFocus: Boolean ->
            mode = if (hasFocus) SurfaceMode.SEARCH else SurfaceMode.DEFAULT
            Log.i(TAG, "Activated SurfaceMode : $mode")
            if (mode == SurfaceMode.DEFAULT) {
                viewModel.fetchSchools()
            }
        }
        val searchCloseButton =
            searchView.findViewById<ImageView>(androidx.appcompat.R.id.search_close_btn)
        searchCloseButton?.setOnClickListener {
            searchView.setQuery("", false)
            searchView.isIconified = true
            searchView.onActionViewCollapsed()
        }
        // Restore the search query when the query is not empty
        if (!query.isNullOrEmpty()) {
            searchView.setQuery(query, false)
            searchView.isIconified = false
        }
    }

    private val searchQueryListener: OnQueryTextListener = object : OnQueryTextListener {
        override fun onQueryTextSubmit(query: String?): Boolean {
            if (TextUtils.isEmpty(query)) {
                return false
            }
            Log.i(TAG, "school search query text $query")
            viewModel.searchSchool(query?.trim() ?: "")
            return true
        }

        override fun onQueryTextChange(newText: String?): Boolean {
            viewModel.filterSchool(newText?.trim() ?: "")
            return true
        }
    }

    override fun observeViewModel() {
        viewModel.schoolListLiveData.observe(
            this,
            Observer { schoolList ->
                binding.swipeRefreshLayout.isRefreshing = false
                if (viewModel.fetchType == NYCSchoolListViewModel.FetchType.LOAD_MORE) {
                    adapter?.addSchools(schoolList)
                    return@Observer
                }
                handleViewsVisibility(schoolList.isNotEmpty())
                adapter?.setSchoolList(schoolList)
            })
        viewModel.searchSchoolLiveData.observe(
            this
        ) { response ->
            handleViewsVisibility(response.isNotEmpty())
            adapter?.setSchoolList(response)
        }
    }

    override fun onRefresh() {
        //refresh data list here then after we get the fetching response, we set the loading state to false.
        Log.i(TAG, "refreshing data")
        viewModel.refreshData()
    }

    private fun startActivitySafely(intent: Intent) {
        try {
            startActivity(intent)
        } catch (e: ActivityNotFoundException) {
            Log.e(TAG, "Failed to start intent : $intent", e)
            // We could display a toast here if the operation fails.
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        if (::searchView.isInitialized) {
            outState.putCharSequence(SEARCH_QUERY_TEXT, searchView.query)
        }
    }

    private fun handleViewsVisibility(shouldShowList: Boolean = true) {
        if (shouldShowList) {
            binding.recyclerView.visibility = View.VISIBLE
            binding.emptyListView.visibility = View.GONE
        } else {
            binding.recyclerView.visibility = View.GONE
            binding.emptyListView.visibility = View.VISIBLE
            binding.emptyListView.text =
                getString(if (mode == SurfaceMode.SEARCH) R.string.empty_result_text else R.string.empty_list_text)
        }

    }


    private enum class SurfaceMode {
        DEFAULT, // State that identify regular unfiltered list display mode
        SEARCH  // State that identify when we are in search mode
    }

    companion object {
        const val TAG = "NycSchoolListFragment"
        const val SEARCH_QUERY_TEXT = "search_query_text"

        @JvmStatic
        fun getWebsiteIntent(school: NYCSchool): Intent {
            val website = StringBuilder(school.website)
            if (website.startsWith("www")) {
                website.insert(0, "https://")
            }
            return Intent(Intent.ACTION_VIEW, Uri.parse(website.toString()))
        }

        @JvmStatic
        fun getPhoneIntent(school: NYCSchool): Intent {
            return Intent(Intent.ACTION_DIAL).apply { data = Uri.parse("tel:${school.phone}") }
        }

        @JvmStatic
        fun getEmailIntent(school: NYCSchool): Intent {
            val intent = Intent(Intent.ACTION_SENDTO)
            intent.data = Uri.parse("mailto:")
            intent.putExtra(Intent.EXTRA_EMAIL, school.email)
           return intent
        }

        @JvmStatic
        fun getDirectionsIntent(school: NYCSchool): Intent {
            val uri = Uri.parse("geo:${school.latitude},${school.longitude}(${school.schoolName})")
            return Intent(Intent.ACTION_VIEW, uri)
        }
    }
}
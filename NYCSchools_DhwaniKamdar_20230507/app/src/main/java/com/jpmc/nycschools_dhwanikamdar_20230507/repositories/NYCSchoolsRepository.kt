package com.jpmc.nycschools_dhwanikamdar_20230507.repositories


import com.jpmc.nycschools_dhwanikamdar_20230507.api.NYCSchoolAPI
import com.jpmc.nycschools_dhwanikamdar_20230507.api.NYCSchoolAPIProvider
import com.jpmc.nycschools_dhwanikamdar_20230507.datamodel.NYCSchool
import com.jpmc.nycschools_dhwanikamdar_20230507.datamodel.SatScore
import com.jpmc.nycschools_dhwanikamdar_20230507.util.Config
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import retrofit2.Response

/**
repository - acts as an intermediary between the ViewModel and the API service.
It fetches data from the API service and provides it to the ViewModel.


NYCSchoolAPIProvider.createRestApi(NYCSchoolAPI::class.java) -  Creates an instance of the API service using Retrofit, which is used to call the API endpoints.

Response<List<T>>.bodyList() - helper function that returns the list of items from the response object or an empty list if the response object is null.
fetchSchoolList() - fetches a list of schools from the API service by passing a query map that contains the limit, offset, and order parameters.
It returns a Flow of the list of NYCSchool objects.

fetchSchoolByName() -  fetches a list of schools from the API service by passing a query map that contains the name parameter. It returns a Flow of the list of NYCSchool objects.
startSchoolListFetching() - fetches a list of schools from the API service by passing the query map to the API endpoint. It returns a Flow of the list of NYCSchool objects. It is a private function that is used internally by fetchSchoolList() and fetchSchoolByName() functions.
fetchSchoolScoreById() -  fetches a list of SAT scores for a school from the API service by passing the school ID as a parameter. It returns a Flow of the list of SatScore objects.

The repository class is designed to work with Kotlin Coroutines and provides the fetched data as Flow objects
It allows the ViewModel to observe the data changes asynchronously.
It also runs its functions on the IO thread using flowOn(Dispatchers.IO), which ensures that the API calls are made asynchronously in the background.
 */

object NYCSchoolsRepository {

    private val schoolService = NYCSchoolAPIProvider.createRestApi(NYCSchoolAPI::class.java)

    private fun <T> Response<List<T>>.bodyList(): List<T> {
        return body() ?: emptyList()
    }

    suspend fun fetchSchoolList(offset: Int): Flow<List<NYCSchool>> {
        val queryMap = HashMap<String, String>().apply {
            put(Config.LIMIT_PARAM, Config.LIMIT_VALUE.toString())
            put(Config.OFFSET_PARAM, offset.toString())
            put(Config.ORDER_PARAM, Config.ORDER_VALUE)
        }
        return startSchoolListFetching(queryMap)
    }

    suspend fun fetchSchoolByName(name: String): Flow<List<NYCSchool>> {
        val queryMap = HashMap<String, String>().apply {
            put(Config.SCHOOL_NAME_PARAM, name)
        }
        return startSchoolListFetching(queryMap)
    }

    private suspend fun startSchoolListFetching(queryMap: HashMap<String, String>): Flow<List<NYCSchool>> {
        return flow { emit(schoolService.getSchoolList(queryMap).bodyList()) }
            .flowOn(Dispatchers.IO)
    }

    suspend fun fetchSchoolScoreById(id: String): Flow<List<SatScore>> {
        val queryMap = HashMap<String, String>().apply {
            put(Config.ID_PARAM, id)
        }
        return flow { emit(schoolService.getSchoolScoreById(queryMap).bodyList()) }
            .flowOn(Dispatchers.IO)
    }
}
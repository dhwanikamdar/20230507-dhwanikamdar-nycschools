package com.jpmc.nycschools_dhwanikamdar_20230507.view

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import androidx.lifecycle.ViewModelProvider
import com.jpmc.nycschools_dhwanikamdar_20230507.R
import com.github.mikephil.charting.data.BarData
import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.data.BarEntry
import com.github.mikephil.charting.utils.ColorTemplate
import com.jpmc.nycschools_dhwanikamdar_20230507.parent.ParentFragment
import com.jpmc.nycschools_dhwanikamdar_20230507.databinding.NycSchoolSatScoreBinding
import com.jpmc.nycschools_dhwanikamdar_20230507.datamodel.NYCSchool
import com.jpmc.nycschools_dhwanikamdar_20230507.datamodel.SatScore
import com.jpmc.nycschools_dhwanikamdar_20230507.viewmodel.NYCSchoolScoresViewModel

/**
NYCSchoolDetailsFragment - represents the UI fragment for displaying SAT score details of a selected school in a chart format.

NYCSchoolScoresViewModel - handles the business logic for fetching the SAT scores of the selected school and updating the UI.
observeViewModel() - observes changes in the ViewModel's schoolScoresLiveData and calls the processChart method to update the chart's data accordingly.

processChart() - takes a list of SatScore objects and uses it to populate the chart with data.
The method creates a BarDataSet object using the SAT scores obtained and sets its colors, text sizes, and text colors.
It then creates a BarData object using the BarDataSet object and sets it to the chart's data. Finally, it animates the chart's data display using animateY.

The class also contains several constants such as SCHOOL_KEY, MATH, READING, WRITING, and TAKERS used throughout the class to provide values for keys and labels.
 */

class NYCSchoolDetailsFragment : ParentFragment() {
    private lateinit var binding: NycSchoolSatScoreBinding
    private lateinit var viewModel: NYCSchoolScoresViewModel
    private var school: NYCSchool? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(this, viewModelFactory)[NYCSchoolScoresViewModel::class.java]
        school = arguments?.getParcelable(SCHOOL_KEY)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.schoolName.text = school?.schoolName
        binding.description.text = school?.description
        viewModel.fetchSchoolScores(school?.id ?: "")
    }

    override fun getActionBarViewData(): ActionBarViewData =
        ActionBarViewData(school?.schoolName ?: getString(R.string.school_information), 0)

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                parentFragmentManager.popBackStackImmediate()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun observeViewModel() {
        viewModel.schoolScoresLiveData.observe(
            this,
            { response -> processChart(response) })
    }

    private fun processChart(scores: List<SatScore>) {
        if (scores.isEmpty()) {
            return
        }
        val score = scores[0]
        val entries: ArrayList<BarEntry> = ArrayList()
        entries.add(BarEntry(1f, score.satMath.toFloatOrNull() ?: 0f))
        entries.add(BarEntry(2f, score.satReading.toFloatOrNull() ?: 0f))
        entries.add(BarEntry(3f, score.satWriting.toFloatOrNull() ?: 0f))
        entries.add(BarEntry(4f, score.satTakers.toFloatOrNull() ?: 0f))

        val barDataSet = BarDataSet(entries, getString(R.string.sat_chart_label))
        barDataSet.colors = ColorTemplate.createColors(ColorTemplate.COLORFUL_COLORS)
        barDataSet.valueTextColor = Color.BLACK
        barDataSet.valueTextSize = 16f

        val barData = BarData(barDataSet)
        binding.chart.setFitBars(true)
        binding.chart.data = barData
        binding.chart.description.text = score.schoolName
        binding.chart.animateY(CHART_ANIM_MS)
    }

    private fun getXAxisValues(): ArrayList<String> {
        val axis: ArrayList<String> = ArrayList()
        axis.add(MATH)
        axis.add(READING)
        axis.add(WRITING)
        axis.add(TAKERS)
        return axis
    }

    override fun initFragmentView(inflater: LayoutInflater): View {
        binding = NycSchoolSatScoreBinding.inflate(inflater)
        return binding.root
    }

    companion object {
        const val TAG = "SchoolDetailsFragment"
        private const val SCHOOL_KEY = "school_key"
        private const val MATH = "Math"
        private const val READING = "Reading"
        private const val WRITING = "Writing"
        private const val TAKERS = "Takers"
        private const val CHART_ANIM_MS = 1000

        @JvmStatic
        fun newInstance(school: NYCSchool): NYCSchoolDetailsFragment {
            return NYCSchoolDetailsFragment().apply {
                val bundle = Bundle()
                bundle.putParcelable(SCHOOL_KEY, school)
                arguments = bundle
            }
        }
    }
}
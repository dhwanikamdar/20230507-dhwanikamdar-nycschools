package com.jpmc.nycschools_dhwanikamdar_20230507.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.jpmc.nycschools_dhwanikamdar_20230507.repositories.NYCSchoolsRepository

/*class NYCSchoolViewModelFactory(
) : ViewModelProvider.Factory {
   override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(NYCSchoolViewModel::class.java)) {
            return NYCSchoolViewModel() as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}*/

class NYCSchoolViewModelFactory : ViewModelProvider.Factory {
    private val repository = NYCSchoolsRepository

    @Suppress("UNCHECKED_CAST") // Safe from type checking
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(NYCSchoolListViewModel::class.java)) {
            return NYCSchoolListViewModel(repository) as T
        } else if (modelClass.isAssignableFrom(NYCSchoolScoresViewModel::class.java)) {
            return NYCSchoolScoresViewModel(repository) as T
        }
        throw IllegalArgumentException("Unknown view model")
    }
}
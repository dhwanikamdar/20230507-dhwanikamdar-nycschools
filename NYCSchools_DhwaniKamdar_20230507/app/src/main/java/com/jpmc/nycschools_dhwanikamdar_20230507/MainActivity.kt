package com.jpmc.nycschools_dhwanikamdar_20230507

import android.os.Bundle
import android.view.LayoutInflater
import androidx.appcompat.app.AppCompatActivity
import com.jpmc.nycschools_dhwanikamdar_20230507.databinding.NycSchoolActivityMainBinding
import com.jpmc.nycschools_dhwanikamdar_20230507.view.NYCSchoolListFragment

// MainActivity - Compose features may be implemented for robust code architecture, it will also reduce boilerplate code big time.

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(NycSchoolActivityMainBinding.inflate(LayoutInflater.from(applicationContext)).root)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .add(R.id.fragment_container, NYCSchoolListFragment(), NYCSchoolListFragment.TAG)
                .commit()
        }
    }
}
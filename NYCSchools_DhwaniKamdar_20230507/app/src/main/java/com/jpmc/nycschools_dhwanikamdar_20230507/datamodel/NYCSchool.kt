package com.jpmc.nycschools_dhwanikamdar_20230507.datamodel

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

/*
data class NYCSchool(
    val dbn: String,
    val name: String,
    val overviewParagraph: String,
    val address: String,
    val phone: String,
    val email: String,
    val website: String,
    val totalStudents: String

    // Additional fields can be added as needed
)

*/

@Parcelize
data class NYCSchool(
    @SerializedName("dbn") val id: String,
    @SerializedName("school_name") val schoolName: String,
    @SerializedName("overview_paragraph") val description: String,
    @SerializedName("location") val address: String,
    @SerializedName("phone_number") val phone: String,
    @SerializedName("website") val website: String,
    @SerializedName("school_email") val email: String,
    @SerializedName("total_students") val numberOfStudents: String,
    @SerializedName("start_time") val startTime: String,
    @SerializedName("end_time") val endTime: String,
    @SerializedName("latitude") val latitude: String,
    @SerializedName("longitude") val longitude: String

) : Parcelable



# 20230507-DhwaniKamdar-NYCSchools

**NYC High Schools App**

This is an Android application developed using Kotlin that displays a list of high schools in New York City. The app uses the 2017 DOE High School Directory data available at https://data.cityofnewyork.us/Education/DOE-High-School-Directory-2017/s3k6-pzi2.

Selecting a school from the list will display additional information about the school, including the SAT scores for Math, Reading, and Writing. The app uses data from the NYC SAT Results available at https://data.cityofnewyork.us/Education/SAT-Results/f9bf-2cp4.

The app is constructed following the MVVM architecture pattern and uses Kotlin coroutines for asynchronous programming, LiveData for reactive programming, Retrofit for making API requests, and a Singleton class for dependency injection. The app follows best practices coding patterns and modern asynchronous patterns for good stability, speedy performance, and error handling. The app also has good UX and uses 3rd party libraries.

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/dhwanikamdar/20230507-dhwanikamdar-nycschools.git
git branch -M main
git push -uf origin main
```

##branching stretagy
    Start with a stable main branch:
        The "main" branch should always contain production-ready code, and be protected from direct pushes and forced changes.
        All changes should be developed and merged to the master branch first, and then later merged to main.

    Create feature branches:
        Developers can create feature branches off of master for new work and bug fixes.
        When the feature branch is complete, a pull request should be created to merge the changes into master.
        Once the changes have been reviewed and tested, they can be merged into master.

    Release branches:
        Release branches can be created from master for preparing a new release of the app.
        The release branch should only contain bug fixes and be well tested before merging back into master.


## Installation
Clone the repository, Check out Master branch and open it in Android Studio. Then, build the app and run it on an emulator or a connected device.

https://gitlab.com/dhwanikamdar/20230507-dhwanikamdar-nycschools.git

## Usage
Upon launching the app, you will be presented with a list of high schools in New York City. Selecting a school from the list will display additional information about the school, including the SAT scores for Math, Reading, and Writing.

## Authors and acknowledgment
This app was developed by Dhwani Kamdar.

